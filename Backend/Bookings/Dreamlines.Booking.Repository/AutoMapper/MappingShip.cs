﻿using AutoMapper;

namespace Dreamlines.Bookings.Repository
{
    internal class MappingShip : Profile
    {
        public MappingShip()
        {
            CreateMap<Ship, Domain.QuartelyBookings.Ship>()
                .ConstructUsing(x => new Domain.QuartelyBookings.Ship(x.Id))
                .ForMember(x => x.Name, o => o.MapFrom(t => t.Name))
                .ForMember(x => x.SaleUnit, o => o.MapFrom(t => t.SaleUnit));
        }
    }
}
