﻿using System.Collections.Generic;
using System.Linq;

namespace Dreamlines.Bookings.Domain.QuartelyBookings
{
    public class SalesBuilder
    {
        private readonly IList<Booking> _bookings;

        public SalesBuilder(IList<Booking> bookings)
        {
            this._bookings = bookings;
        }

        public List<SaleUnit> Build()
        {
            return _bookings?.GroupBy(x => x.Ship.SaleUnit)
                .Select(x =>
                    new SaleUnit(x.Key.Id)
                        .SetName(x.Key.Name)
                        .SetCountry(x.Key.Country)
                        .SetCurrency(x.Key.Currency)
                        .SetBookings(x.AsEnumerable())
                )
                .ToList();
        }
    }
}
