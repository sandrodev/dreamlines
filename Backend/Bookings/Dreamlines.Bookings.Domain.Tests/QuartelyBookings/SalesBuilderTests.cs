﻿using Dreamlines.Bookings.Domain.QuartelyBookings;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Dreamlines.Bookings.Domain.Tests.QuartelyBookings
{
    public class SalesBuilderTests
    {
        [Fact]
        public void GivenBookingsIsNull_ShouldReturnNull()
        {
            var result = new SalesBuilder(bookings: null).Build();
            Assert.Null(result);
        }

        [Fact]
        public void GivenItHasMultipleBookingsForSameSale_ShouldGroupBookingsBySale()
        {
            var saleA = new SaleUnit(1).SetName("test1").SetCountry("Brazil").SetCurrency("R$");
            var saleB = new SaleUnit(2).SetName("test2").SetCountry("Argentina").SetCurrency("$");

            var shipBrazil = new Ship(1).SetSaleUnit(saleA);
            var shipMexico = new Ship(3).SetSaleUnit(saleA);
            var shipArgentina = new Ship(2).SetSaleUnit(saleB);

            var bookings = new List<Booking>()
            {
                new Booking(1).SetShip(shipBrazil),
                new Booking(3).SetShip(shipArgentina),
                new Booking(2).SetShip(shipMexico),
            };

            var sales = new SalesBuilder(bookings).Build();
            Assert.Equal(2, sales.Count);
            Assert.Equal(2, sales[0].Bookings.Count());
            Assert.Equal(1, sales[1].Bookings.Count());
        }
    }
}
