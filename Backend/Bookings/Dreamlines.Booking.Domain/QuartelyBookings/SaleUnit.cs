﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dreamlines.Bookings.Domain.QuartelyBookings
{
    public sealed class SaleUnit : Entity
    {
        public SaleUnit(int id) : base(id) { }

        public string Name { get; private set; }
        public string Country { get; private set; }
        public string Currency { get; private set; }
        public IEnumerable<Booking> Bookings { get; private set; }
        public decimal TotalPrice => Bookings?.ToList().Sum(x => x.Price) ?? 0;

        public SaleUnit SetName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Sale Unit Name cannot be empty.", nameof(Name));

            this.Name = name;
            return this;
        }

        public SaleUnit SetCountry(string country)
        {
            if (string.IsNullOrEmpty(country))
                throw new ArgumentException("Sale Unit Country cannot be empty.", nameof(Country));

            this.Country = country;
            return this;
        }

        public SaleUnit SetCurrency(string currency)
        {
            if (string.IsNullOrEmpty(currency))
                throw new ArgumentException("Sale Unit Currency cannot be empty.", nameof(Currency));

            this.Currency = currency;
            return this;
        }

        public SaleUnit SetBookings(IEnumerable<Booking> bookings)
        {
            this.Bookings = bookings;
            return this;
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (this.GetType() != other.GetType())
                return false;
            return this.Id.Equals(((SaleUnit)other).Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
