import { Component, Input } from '@angular/core';
import Booking from '../../entities/booking';

@Component({
  selector: 'app-bookings-table',
  templateUrl: './bookings-table.component.html'
})
export default class BookingsTableComponent {
  @Input() currency: string;
  @Input() bookings: Booking[];
}
