﻿using System.Collections.Generic;
using System.Linq;
using Dreamlines.Bookings.Domain.Assertions;

namespace Dreamlines.Bookings.Web.Bootstrap
{
    public sealed class InMemoryNotificationsContainer : INotificationContainer
    {
        private readonly List<Notification> _notifications;

        public InMemoryNotificationsContainer()
        {
            this._notifications = new List<Notification>();
        }

        public INotificationContainer Assert(IValidatable validatable)
        {
            var messages = validatable.Validate();
            if(messages.Any())
            {
                _notifications.AddRange(messages);
            }
            return this;
        }

        public List<Notification> GetNotifications()
        {
            return new List<Notification>(_notifications);
        }

        public bool IsInvalid() => !IsValid();

        public bool IsValid()
        {
            return !this._notifications.Any(x => x.MessageType == MessageType.Error);
        }
    }
}
