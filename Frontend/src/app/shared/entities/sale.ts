import Booking from './booking';

export default interface Sale {
  id: number;
  name: string;
  country: string;
  currency: string;
  bookings: Booking[];
  totalPrice: number;
}
