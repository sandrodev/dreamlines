import { Component, Input } from '@angular/core';
import Sale from '../../entities/sale';

@Component({
  selector: 'app-sale-card',
  templateUrl: './sale-card.component.html',
  styleUrls: ['./sale-card.component.scss']
})
export default class SaleCardComponent {
  @Input() sale: Sale;
  clicked = false;

  toogleBookings() {
    this.clicked = !this.clicked;
  }
}
