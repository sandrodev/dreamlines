﻿using System.Collections.Generic;

namespace Dreamlines.Bookings.Domain.QuartelyBookings
{
    public interface ISalesService
    {
        IList<SaleUnit> ListSales(SearchBooking request);
    }
}
