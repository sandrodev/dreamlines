﻿using System.Threading.Tasks;
using Dreamlines.Bookings.Domain.Assertions;
using Dreamlines.Bookings.Domain.QuartelyBookings;
using Microsoft.AspNetCore.Mvc;

namespace Dreamlines.Bookings.Web.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    public class SalesController : ControllerBase
    {
        private readonly ISalesService _service;
        private readonly INotificationContainer _notifications;

        public SalesController(ISalesService service, INotificationContainer notifications)
        {
            this._service = service;
            this._notifications = notifications;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]SearchBooking request)
        {
            var result = _service.ListSales(request);
            if(_notifications.IsInvalid())
            {
                return BadRequest(_notifications.GetNotifications());
            }
            return Ok(result);
        }
    }
}
