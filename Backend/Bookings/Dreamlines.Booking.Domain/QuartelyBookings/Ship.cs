﻿using System;

namespace Dreamlines.Bookings.Domain.QuartelyBookings
{
    public sealed class Ship : Entity
    {
        public Ship(int id) : base(id) { }
        public string Name { get; private set; }
        public SaleUnit SaleUnit { get; private set; }

        public Ship SetName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Ship Name cannot be empty.", nameof(name));

            this.Name = name;
            return this;
        }

        public Ship SetSaleUnit(SaleUnit saleUnit)
        {
            this.SaleUnit = saleUnit;
            return this;
        }
    }
}
