import { Component, Input, Output, EventEmitter } from '@angular/core';
import SalesSearchRequest from '../../transition-objects/salesSearchRequest';

@Component({
  selector: 'app-sale-filters',
  templateUrl: './sale-filters.component.html',
  styleUrls: ['./sale-filters.component.scss']
})
export default class SaleFiltersComponent {
  @Input() model: SalesSearchRequest = new SalesSearchRequest();
  @Output() modelChanged = new EventEmitter<SalesSearchRequest>();

  onChangeModel($event, property) {
    this.model[property] = $event.target.value;
  }

  onSearchClick() {
    this.modelChanged.emit(this.model);
  }
}
