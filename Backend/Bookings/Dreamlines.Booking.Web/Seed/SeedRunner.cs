﻿using Dreamlines.Bookings.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Linq;

namespace Dreamlines.Bookings.Web.Seed
{
    internal sealed class SeedRunner
    {
        public static void Run(BookingsContext context)
        {
            if (context.SaleUnits.Any())
                return;

            var jsonContent = File.ReadAllText("Seed/TrialDayData.json");
            SeedModel model = JsonConvert.DeserializeObject<SeedModel>(jsonContent, new JsonSerializerSettings() { 
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
            context.SaleUnits.AddRange(model.SalesUnits);
            context.Ships.AddRange(model.Ships);
            context.Bookings.AddRange(model.Bookings);
            context.SaveChanges();
        }
    }
}
