﻿using Dreamlines.Bookings.Domain.QuartelyBookings;
using System;
using System.Linq;
using Xunit;

namespace Dreamlines.Bookings.Domain.Tests.QuartelyBookings
{
    public class SearchBookingTests
    {
        [Fact]
        public void GivenTheInitialDateIsNotSet_ShouldContainsAnError()
        {
            var validations = new SearchBooking()
            {
                InitialDate = DateTime.MinValue,
                EndDate = DateTime.Now,
                Pagination = new Pagination(),
            }
            .Validate();

            Assert.True(validations.Count() == 1);
            Assert.True(validations.First().MessageType == Assertions.MessageType.Error);
            Assert.True(validations.First().Title.Equals("Invalid period range."));
        }

        [Fact]
        public void GivenTheEndDateIsNotSet_ShouldContainsAnError()
        {
            var validations = new SearchBooking()
            {
                InitialDate = DateTime.Now,
                EndDate = DateTime.MinValue,
                Pagination = new Pagination(),
            }
            .Validate();

            Assert.True(validations.Count() == 1);
            Assert.True(validations.First().MessageType == Assertions.MessageType.Error);
            Assert.True(validations.First().Title.Equals("Invalid period range."));
        }

        [Fact]
        public void GivenTheEndDateIsBeforeTheStartDate_ShouldContainsAnError()
        {
            var validations = new SearchBooking()
            {
                InitialDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(-5),
                Pagination = new Pagination(),
            }
            .Validate();

            Assert.True(validations.Count() == 1);
            Assert.True(validations.First().MessageType == Assertions.MessageType.Error);
            Assert.True(validations.First().Title.Equals("Invalid period range."));
        }

        [Fact]
        public void GivenThePaginationIsNull_ShouldContainsAnError()
        {
            var validations = new SearchBooking()
            {
                InitialDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(1),
                Pagination = null,
            }
            .Validate();

            Assert.True(validations.Count() == 1);
            Assert.True(validations.First().MessageType == Assertions.MessageType.Error);
            Assert.True(validations.First().Title.Equals("Invalid pagination params."));
        }

        [Fact]
        public void GivenThePaginationSkipAndTakeAreZero_ShouldContainsAnError()
        {
            var validations = new SearchBooking()
            {
                InitialDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(1),
                Pagination = new Pagination()
                {
                    Take = 0,
                    Skip = 0
                },
            }
            .Validate();

            Assert.True(validations.Count() == 1);
            Assert.True(validations.First().MessageType == Assertions.MessageType.Error);
            Assert.True(validations.First().Title.Equals("Invalid pagination params."));
        }

        [Fact]
        public void GiveSearchBookingsIsValid_ShouldsContainsAnyMessage()
        {
            var validations = new SearchBooking()
            {
                InitialDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(1),
                Pagination = new Pagination()
                {
                    Skip = 0,
                    Take = 10
                },
            }
            .Validate();

            Assert.False(validations.Any());
        }
    }
}
