import { BookingAdapter } from './booking-adapter';
import { Injectable } from '@angular/core';
import { Adapter } from './adapter';
import Sale from '../entities/sale';
import Booking from '../entities/booking';

class SaleEntity implements Sale {
  id: number;
  name: string;
  country: string;
  currency: string;
  bookings: Booking[];
  totalPrice: number;

  constructor(item) {
    this.id = item.id;
    this.name = item.name;
    this.country = item.country;
    this.currency = item.currency;
    this.totalPrice = item.totalPrice;

    if (item.bookings != null) {
      const adapter = new BookingAdapter();
      this.bookings = item.bookings.map(adapter.adapt);
    }
  }
}

@Injectable({
    providedIn: 'root'
})
export class SaleAdapter implements Adapter<Sale> {
  adapt(item: any): Sale {
    return new SaleEntity(item);
  }
}
