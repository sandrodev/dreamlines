﻿using AutoMapper;

namespace Dreamlines.Bookings.Repository
{
    internal class MappingSaleUnit : Profile
    {
        public MappingSaleUnit()
        {
            CreateMap<SaleUnit, Domain.QuartelyBookings.SaleUnit>()
                .ConstructUsing(x => new Domain.QuartelyBookings.SaleUnit(x.Id))
                .ForMember(x => x.Name, o => o.MapFrom(t => t.Name))
                .ForMember(x => x.Country, o => o.MapFrom(t => t.Country))
                .ForMember(x => x.Currency, o => o.MapFrom(t => t.Currency));
        }
    }
}
