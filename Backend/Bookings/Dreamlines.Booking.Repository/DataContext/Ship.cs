﻿using System.Collections.Generic;

namespace Dreamlines.Bookings.Repository
{
    public class Ship
    {
        public int Id { get; set; }
        public int SalesUnitId { get; set; }
        public string Name { get; set; }

        public virtual SaleUnit SaleUnit { get; set; }

        public virtual IEnumerable<Booking> Bookings { get; set; }
    }
}
