import { Injectable } from '@angular/core';
import { Adapter } from './adapter';
import Ship from '../entities/ship';

class ShipEntity implements Ship {
  id: number;
  name: string;

  constructor(item) {
    this.id = item.id;
    this.name = item.name;
  }
}

@Injectable({
    providedIn: 'root'
})
export class ShipAdapter implements Adapter<Ship> {
  adapt(item: any): Ship {
    return new ShipEntity(item);
  }
}
