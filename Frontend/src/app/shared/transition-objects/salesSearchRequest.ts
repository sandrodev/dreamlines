export default class SalesSearchRequest {
  constructor() {
    this.initialDate = '2016-01-01';
    this.endDate = '2016-03-31';
    this.search = '';
  }

  initialDate: string;
  endDate: string;
  search: string;
}
