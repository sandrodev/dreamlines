﻿using Dreamlines.Bookings.Domain.Assertions;
using Dreamlines.Bookings.Domain.QuartelyBookings;
using NSubstitute;
using System.Collections.Generic;
using Xunit;

namespace Dreamlines.Bookings.Domain.Tests.QuartelyBookings.Service
{
    public class SalesServiceTests
    {
        [Fact]
        public void GivenRequestIsInvalid_ShouldNotCallRepository_AndReturnNull()
        {
            var repository = Substitute.For<IBookingsRepository>();
            var assertions = Substitute.For<INotificationContainer>();

            repository.SearchBookings(Arg.Any<SearchBooking>()).Returns(new List<Booking>());

            assertions.Assert(Arg.Any<IValidatable>()).IsValid().Returns(false);

            var service = new SalesService(repository, assertions);
            var bookings = service.ListSales(new SearchBooking());

            repository.DidNotReceiveWithAnyArgs().SearchBookings(default);
            Assert.Null(bookings);
        }

        [Fact]
        public void GivenRequestIsValid_ShouldCallRepository_AndReturnTheBookings()
        {
            var repository = Substitute.For<IBookingsRepository>();
            var assertions = Substitute.For<INotificationContainer>();
            var saleUnit = new SaleUnit(6).SetName("name").SetCurrency("R$").SetCountry("Brazil");
            var bookings = new List<Booking>() { 
                new Booking(10)
                    .SetShip(new Ship(2)
                    .SetSaleUnit(saleUnit)) };

            repository.SearchBookings(Arg.Any<SearchBooking>()).Returns(bookings);

            assertions.Assert(Arg.Any<IValidatable>()).IsValid().Returns(true);

            var service = new SalesService(repository, assertions);
            var result = service.ListSales(new SearchBooking());

            repository.ReceivedWithAnyArgs().SearchBookings(default);
            Assert.NotNull(result);
        }
    }
}
