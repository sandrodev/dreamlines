﻿using System.Collections.Generic;

namespace Dreamlines.Bookings.Domain.Assertions
{
    public interface IValidatable
    {
        IList<Notification> Validate();
    }
}
