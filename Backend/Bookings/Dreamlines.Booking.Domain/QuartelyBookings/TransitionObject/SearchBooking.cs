﻿using Dreamlines.Bookings.Domain.Assertions;
using System;
using System.Collections.Generic;

namespace Dreamlines.Bookings.Domain.QuartelyBookings
{
    public sealed class SearchBooking : IValidatable
    {
        public SearchBooking()
        {
            Pagination = new Pagination();
        }

        public DateTime InitialDate { get; set; }
        public DateTime EndDate { get; set; }
        public int BookingId { get; set; }
        public string ShipName { get; set; }
        public Pagination Pagination { get; set; }

        public IList<Notification> Validate()
        {
            var messages = new List<Notification>();

            if (InitialDate == DateTime.MinValue || EndDate == DateTime.MinValue || EndDate < InitialDate)
                messages.Add(Notification.Error("Invalid period range."));

            if (Pagination == null || (Pagination.Skip <= 0 && Pagination.Take <= 0))
                messages.Add(Notification.Error("Invalid pagination params."));

            return messages;
        }
    }
}
