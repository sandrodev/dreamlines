import Ship from './ship';

export default interface Booking {
  id: number;
  date: Date;
  price: number;
  ship: Ship;
}
