﻿using Dreamlines.Bookings.Domain.QuartelyBookings;
using System;
using Xunit;

namespace Dreamlines.Bookings.Domain.Tests.QuartelyBookings
{
    public class BookingTests
    {
        [Fact]
        public void GivenIdIsZero_ThrowsAnArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => new Booking(id: 0));
            Assert.Equal("Id cannot be less or equal to 0. (Parameter 'Id')", exception.Message);
        }

        [Fact]
        public void GivenDateIsEmpty_ThrowsAnArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => new Booking(id: 10).SetDate(DateTime.MinValue));
            Assert.Equal("Booking Date cannot be empty. (Parameter 'Date')", exception.Message);
        }

        [Fact]
        public void GivenPriceIsNegative_ThrowsAnArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => new Booking(id: 10).SetPrice(-100));
            Assert.Equal("Price cannot be negative. (Parameter 'Price')", exception.Message);
        }

        [Fact]
        public void GivenShipIsValid_ShouldNotThrowAnyException()
        {
            var id = 2;
            var date = DateTime.Now;
            var price = 99;

            var booking = new Booking(id)
                .SetDate(date)
                .SetPrice(price);

            Assert.Equal(id, booking.Id);
            Assert.Equal(date, booking.Date);
            Assert.Equal(price, booking.Price);
        }
    }
}
