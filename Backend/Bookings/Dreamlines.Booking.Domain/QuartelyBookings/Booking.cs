﻿using System;

namespace Dreamlines.Bookings.Domain.QuartelyBookings
{
    public sealed class Booking : Entity
    {
        public Booking(int id) : base(id) { }

        public Ship Ship { get; private set; }
        public DateTime Date { get; private set; }
        public decimal Price { get; private set; }

        public Booking SetShip(Ship ship)
        {
            this.Ship = ship;
            return this;
        }

        public Booking SetDate(DateTime date)
        {
            if (date == DateTime.MinValue)
                throw new ArgumentException("Booking Date cannot be empty.", nameof(Date));

            this.Date = date;
            return this;
        }

        public Booking SetPrice(decimal price)
        {
            if (price < 0)
                throw new ArgumentException("Price cannot be negative.", nameof(Price));

            this.Price = price;
            return this;
        }
    }
}
