﻿using AutoMapper;

namespace Dreamlines.Bookings.Repository
{
    public static class Profiles
    {
        public static void Register(IMapperConfigurationExpression configuration)
        {
            configuration.AddProfile(new MappingShip());
            configuration.AddProfile(new MappingSaleUnit());
            configuration.AddProfile(new MappingBooking());
        }
    }
}
