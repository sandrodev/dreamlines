﻿using AutoMapper;
using Dreamlines.Bookings.Domain.Assertions;
using Dreamlines.Bookings.Domain.QuartelyBookings;
using Dreamlines.Bookings.Repository;
using Dreamlines.Bookings.Web.Bootstrap;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Dreamlines.Bookings.Web
{
    internal static class Container
    {
        public static void Setup(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));
            services.AddScoped<INotificationContainer, InMemoryNotificationsContainer>();

            RegisterServices(services);
            RegisterRepositories(services);
        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<ISalesService, SalesService>();
        }

        private static void RegisterRepositories(IServiceCollection services)
        {
            services.AddDbContext<BookingsContext>(options =>
                options.UseSqlServer(System.Environment.GetEnvironmentVariable("CONNECTION_STRING")));

            services.AddSingleton(provider => new MapperConfiguration(cfg =>
            {
                Repository.Profiles.Register(cfg);

            }).CreateMapper());

            services.AddScoped<IBookingsRepository, BookingsRepository>();
        }
    }
}
