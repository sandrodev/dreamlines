﻿using Microsoft.EntityFrameworkCore;

namespace Dreamlines.Bookings.Repository
{
    public sealed class BookingsContext : DbContext
    {
        public BookingsContext(DbContextOptions<BookingsContext> options) 
            : base(options)
        {
            Database.EnsureCreated();
            Database.Migrate();
        }

        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Ship> Ships { get; set; }
        public DbSet<SaleUnit> SaleUnits { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BookingMap());
            modelBuilder.ApplyConfiguration(new SaleUnitMap());
            modelBuilder.ApplyConfiguration(new ShipMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}
