﻿using System.Collections.Generic;

namespace Dreamlines.Bookings.Repository
{
    public class SaleUnit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Currency { get; set; }

        public virtual IEnumerable<Ship> Ships { get; set; }
    }
}
