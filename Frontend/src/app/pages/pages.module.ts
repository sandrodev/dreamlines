import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import HomePageComponent from './home/home.component';

@NgModule({
  declarations: [
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    SharedModule
  ],
  exports: [
    HomePageComponent
  ]
})
export class PagesModule { }
