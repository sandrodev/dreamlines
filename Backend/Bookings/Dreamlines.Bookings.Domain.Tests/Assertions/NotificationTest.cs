using Dreamlines.Bookings.Domain.Assertions;
using System;
using Xunit;

namespace Dreamlines.Bookings.Domain.Tests
{
    public class NotificationTest
    {
        [Fact]
        public void GivenTitleIsEmpty_ThrowsAnArgumentException()
        {
            Assert.Throws<ArgumentException>(() => Notification.Info(string.Empty));
        }

        [Fact]
        public void GivenMessageIsInfo_ReturnsAnMessageOfInfoType()
        {
            Assert.Equal(MessageType.Info, Notification.Info("message").MessageType);
        }

        [Fact]
        public void GivenMessageIsError_ReturnsAnMessageOfErrorType()
        {
            Assert.Equal(MessageType.Error, Notification.Error("message").MessageType);
        }
    }
}
