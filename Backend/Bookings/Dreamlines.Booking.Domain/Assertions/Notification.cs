﻿using System;

namespace Dreamlines.Bookings.Domain.Assertions
{
    public class Notification
    {
        private Notification() { }

        private static Notification New(string title, string content, MessageType messageType)
        {
            if (string.IsNullOrWhiteSpace(title))
                throw new ArgumentException("Message Title cannot be empty.");

            return new Notification()
            {
                Title = title,
                Content = content,
                MessageType = messageType
            };
        }

        public static Notification Info(string title, string content = null)
            => New(title, content, MessageType.Info);

        public static Notification Error(string title, string content = null)
            => New(title, content, MessageType.Error);

        public string Title { get; private set; }
        public string Content { get; private set; }
        public MessageType MessageType { get; private set; }
    }

    public enum MessageType
    {
        Info,
        Error
    }
}
