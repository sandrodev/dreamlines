import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import HeaderComponent from './components/header/header.component';
import SaleCardComponent from './components/sale-card/sale-card.component';
import BookingsTableComponent from './components/bookings-table/bookings-table.component';
import SaleFiltersComponent from './components/sale-filters/sale-filters.component';

@NgModule({
  declarations: [
    HeaderComponent,
    SaleCardComponent,
    SaleFiltersComponent,
    BookingsTableComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule
  ],
  exports: [
    HeaderComponent,
    SaleCardComponent,
    SaleFiltersComponent,
    BookingsTableComponent
  ]
})
export class SharedModule { }
