﻿using System;

namespace Dreamlines.Bookings.Domain.QuartelyBookings
{
    public abstract class Entity
    {
        public Entity(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id cannot be less or equal to 0.", nameof(Id));

            this.Id = id;
        }

        public int Id { get; }
    }
}
