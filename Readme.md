## Getting started

1. Clone the project
2. Edit the `docker-compose-yml`, line 49, replace `http://192.168.99.101:5000` by `http://[your-docker-host-url]:5000`, which is the Api container url
3. Inside the project folder run `make start` to create the docker containers (OR you can run `docker-compose up`).
4. It'll start the `ASP.NET CORE`, `Angular 8` and `SQL Server database`
5. The Seed process will be executed in background and it could take up to 30 seconds to be completed.
6. Open your browser and access the URL `http://[docker-host-url]:4200`


## Preview

![Alt Text](https://bitbucket.org/sandrodev/dreamlines/src/master/Frontend/preview.gif)
