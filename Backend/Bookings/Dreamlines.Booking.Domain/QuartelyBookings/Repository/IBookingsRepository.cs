﻿using System.Collections.Generic;

namespace Dreamlines.Bookings.Domain.QuartelyBookings
{
    public interface IBookingsRepository
    {
        IList<Booking> SearchBookings(SearchBooking searchBookings);
    }
}
