﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Dreamlines.Bookings.Domain.QuartelyBookings;
using Microsoft.EntityFrameworkCore;

namespace Dreamlines.Bookings.Repository
{
    public sealed class BookingsRepository : IBookingsRepository
    {
        private readonly BookingsContext _context;
        private readonly IMapper _mapper;

        public BookingsRepository(BookingsContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public IList<Domain.QuartelyBookings.Booking> SearchBookings(SearchBooking model)
        {
            var query = _context.Bookings
                    .Include(x => x.Ship)
                    .ThenInclude(x => x.SaleUnit);

            var bookings = query.Where(b =>
                b.BookingDate >= model.InitialDate &&
                b.BookingDate <= model.EndDate &&
                (string.IsNullOrEmpty(model.ShipName) || b.Ship.Name.ToLower().Contains(model.ShipName.ToLower())) &&
                (model.BookingId == 0 || b.Id == model.BookingId)
            )
            .ToList();

            return _mapper.Map<IList<Domain.QuartelyBookings.Booking>>(bookings);
        }
    }
}
