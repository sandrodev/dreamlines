﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dreamlines.Bookings.Repository
{
    internal class ShipMap : IEntityTypeConfiguration<Ship>
    {
        public void Configure(EntityTypeBuilder<Ship> builder)
        {
            builder.ToTable("Ship");
            builder.HasKey(x => x.Id);

            builder.Property(c => c.Id)
                .HasColumnName("Id")
                .ValueGeneratedNever();

            builder.HasOne(x => x.SaleUnit)
                .WithMany(x => x.Ships)
                .HasForeignKey(x => x.SalesUnitId);
        }
    }
}
