import { SalesService } from './../../shared/services/salesService';
import { Component, OnInit } from '@angular/core';
import Sale from 'src/app/shared/entities/sale';
import SalesSearchRequest from 'src/app/shared/transition-objects/salesSearchRequest';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export default class HomePageComponent implements OnInit {
  sales: Sale[] = [];
  salesRequest: SalesSearchRequest = new SalesSearchRequest();

  constructor(private service: SalesService) {}

  ngOnInit() {
    this.getSales();
  }

  onModelChanged(request: SalesSearchRequest) {
    this.salesRequest = request;
    this.getSales();
  }

  getSales() {
    this.service.getSales(this.salesRequest).subscribe((data) => {
      this.sales = data;
    });
  }
}
