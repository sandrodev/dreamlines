﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dreamlines.Bookings.Repository
{
    internal class SaleUnitMap : IEntityTypeConfiguration<SaleUnit>
    {
        public void Configure(EntityTypeBuilder<SaleUnit> builder)
        {
            builder.ToTable("SaleUnit");
            builder.HasKey(x => x.Id);

            builder.Property(c => c.Id)
                .HasColumnName("Id")
                .ValueGeneratedNever();
        }
    }
}
