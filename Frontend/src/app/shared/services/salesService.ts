import { SaleAdapter } from './../adapters/sale-adapter';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import Sale from '../entities/sale';
import SalesSearchRequest from '../transition-objects/salesSearchRequest';

declare var ENDPOINT_URL;

@Injectable({
  providedIn: 'root'
})
export class SalesService {
  constructor(
    private http: HttpClient,
    private saleAdapter: SaleAdapter
  ) { }

  getSales(request: SalesSearchRequest): Observable<Sale[]> {
    const params = new HttpParams()
      .set('initialDate', request.initialDate)
      .set('endDate', request.endDate)
      .set('shipName', request.search);

    const options = { params };
    return this.http.get(`${ENDPOINT_URL}/api/sales`, options).pipe(
      map((data: any[]) => data.map(this.saleAdapter.adapt)));
  }
}
