﻿using AutoMapper;

namespace Dreamlines.Bookings.Repository
{
    internal class MappingBooking : Profile
    {
        public MappingBooking()
        {
            CreateMap<Booking, Domain.QuartelyBookings.Booking>()
                .ConstructUsing(x => new Domain.QuartelyBookings.Booking(x.Id))
                .ForMember(x => x.Date, o => o.MapFrom(t => t.BookingDate))
                .ForMember(x => x.Price, o => o.MapFrom(t => t.Price))
                .ForMember(x => x.Ship, o => o.MapFrom(t => t.Ship));
        }
    }
}
