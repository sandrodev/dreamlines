﻿using Dreamlines.Bookings.Repository;
using System.Collections.Generic;

namespace Dreamlines.Bookings.Web.Seed
{
    public class SeedModel
    {
        public List<SaleUnit> SalesUnits { get; set; }
        public List<Ship> Ships { get; set; }
        public List<Booking> Bookings { get; set; }
    }
}
