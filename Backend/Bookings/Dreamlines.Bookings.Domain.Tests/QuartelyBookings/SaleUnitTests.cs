﻿using Dreamlines.Bookings.Domain.QuartelyBookings;
using System;
using System.Collections.Generic;
using Xunit;

namespace Dreamlines.Bookings.Domain.Tests.QuartelyBookings
{
    public class SaleUnitTests
    {
        [Fact]
        public void GivenIdIsZero_ThrowsAnArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => new SaleUnit(id: 0));
            Assert.Equal("Id cannot be less or equal to 0. (Parameter 'Id')", exception.Message);
        }

        [Fact]
        public void GivenNameIsEmpty_ThrowsAnArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => new SaleUnit(id: 10).SetName(string.Empty));
            Assert.Equal("Sale Unit Name cannot be empty. (Parameter 'Name')", exception.Message);
        }

        [Fact]
        public void GivenCountryIsEmpty_ThrowsAnArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => new SaleUnit(id: 10).SetCountry(string.Empty));
            Assert.Equal("Sale Unit Country cannot be empty. (Parameter 'Country')", exception.Message);
        }

        [Fact]
        public void GivenCurrencyIsEmpty_ThrowsAnArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => new SaleUnit(id: 10).SetCurrency(string.Empty));
            Assert.Equal("Sale Unit Currency cannot be empty. (Parameter 'Currency')", exception.Message);
        }

        [Fact]
        public void GivenSaleUnitIsValid_ShouldNotThrowAnyException()
        {
            var id = 2;
            var name = "test";
            var country = "germany";
            var currency = "€";

            var saleUnit = new SaleUnit(id)
                .SetName(name)
                .SetCountry(country)
                .SetCurrency(currency);

            Assert.Equal(id, saleUnit.Id);
            Assert.Equal(name, saleUnit.Name);
            Assert.Equal(country, saleUnit.Country);
            Assert.Equal(currency, saleUnit.Currency);
        }

        [Fact]
        public void GivenSaleUnitHasBookings_ShouldSumTheBookingsTotalPrice()
        {
            var id = 2;
            var bookings = new List<Booking>()
            {
                new Booking(1).SetPrice(100),
                new Booking(2).SetPrice(100),
            };

            var saleUnit = new SaleUnit(id).SetBookings(bookings);
            Assert.Equal(200, saleUnit.TotalPrice);
        }
    }
}
