﻿using Dreamlines.Bookings.Domain.Assertions;
using System.Collections.Generic;

namespace Dreamlines.Bookings.Domain.QuartelyBookings
{
    public sealed class SalesService : ISalesService
    {
        private readonly IBookingsRepository _repository;
        private readonly INotificationContainer _assertions;

        public SalesService(IBookingsRepository repository, INotificationContainer assertion)
        {
            this._repository = repository;
            this._assertions = assertion;
        }

        public IList<SaleUnit> ListSales(SearchBooking request)
        {
            if (!_assertions.Assert(request).IsValid())
                return null;

            var bookings = _repository.SearchBookings(request);
            return new SalesBuilder(bookings).Build();
        }
    }
}
