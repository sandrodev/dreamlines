﻿using System.Collections.Generic;

namespace Dreamlines.Bookings.Domain.Assertions
{
    public interface INotificationContainer
    {
        INotificationContainer Assert(IValidatable validatable);
        bool IsValid();
        bool IsInvalid();
        List<Notification> GetNotifications();
    }
}
