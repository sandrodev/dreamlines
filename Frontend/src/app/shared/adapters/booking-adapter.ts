import { ShipAdapter } from './ship-adapter';
import { Injectable } from '@angular/core';
import { Adapter } from './adapter';
import Booking from '../entities/booking';
import Ship from '../entities/ship';

class BookingEntity implements Booking {
  id: number;
  date: Date;
  price: number;
  ship: Ship;

  constructor(item) {
    this.id = item.id;
    this.price = item.price;

    if (item.ship != null) {
      const adapter = new ShipAdapter();
      this.ship = adapter.adapt(item.ship);
    }
  }
}

@Injectable({
    providedIn: 'root'
})
export class BookingAdapter implements Adapter<Booking> {
  adapt(item: any): Booking {
    return new BookingEntity(item);
  }
}
