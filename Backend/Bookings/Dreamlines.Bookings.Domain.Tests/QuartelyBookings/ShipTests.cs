﻿using Dreamlines.Bookings.Domain.QuartelyBookings;
using System;
using Xunit;

namespace Dreamlines.Bookings.Domain.Tests.QuartelyBookings
{
    public class ShipTests
    {
        [Fact]
        public void GivenIdIsZero_ThrowsAnArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => new Ship(id: 0));
            Assert.Equal("Id cannot be less or equal to 0. (Parameter 'Id')", exception.Message);
        }

        [Fact]
        public void GivenNameIsEmpty_ThrowsAnArgumentException()
        {
            var exception = Assert.Throws<ArgumentException>(() => new Ship(id: 10).SetName(string.Empty));
            Assert.Equal("Ship Name cannot be empty. (Parameter 'name')", exception.Message);
        }

        [Fact]
        public void GivenShipIsValid_ShouldNotThrowAnyException()
        {
            var id = 2;
            var name = "test";

            var ship = new Ship(id).SetName(name);

            Assert.Equal(id, ship.Id);
            Assert.Equal(name, ship.Name);
        }
    }
}
